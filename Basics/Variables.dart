void main() {
  ///This Variable called number stores the value 10
  var number = 10;
  print(number);

  ///a and b varibles storing values
  var a = 5;
  var b = 7;

  ///Performing arithmatic operations such as add, mul, sub, divide(Q) and modulus(R)
  print('a + b = ${a + b}');
  print('a * b = ${a * b}');
  print('a - b = ${a - b}');
  print('a / b = ${a / b}');
  print('a % b = ${a % b}');

  ///Some more operations with variables
  var limit = 16.9;

  ///Here we are adding limit + 3.1 -> the current value of limit is 16.9
  ///Therefore limit = 16.9 + 3.1 => 20.0
  limit = limit + 3.1;
  print('Limit = $limit');

  var counter = 254;

  ///Here we are adding +1 to counter which is counter = counter + 1
  ///shorthanded as counter++
  counter++;
  print('Counter = $counter');

  ///Types of variables aka Datatypes
  bool truth = true;
  int integer = -91;
  double pi = 3.13;
  String helloWorld = "Hello World";

  ///Advanced Datatypes pending - Start
  ///Advanced Datatypes pending - End
}
