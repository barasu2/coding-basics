///Arrays are nothing but Set of similar values
///Or simply say collection of values in the same variable name
void main() {
  ///This is an array of 1 - 6
  var arrNumbers = [1, 2, 3, 4, 5, 6];

  ///This is an array of words
  var arrWords = ['Apple', 'Banana', 'Orange'];

  ///This is an array of boolean
  var arrTruth = [true, false, false, false, true];

  ///Access values in an array

  ///We are trying to get the first value in the arrWords which is "Apple"
  ///NOTE: Arrays always start at Index 0 and not 1 like in Math.
  print(arrWords[0]);

  ///We can also replace a value in an array using the index
  ///Here replacing Apple to Peach
  arrWords[0] = "Peach";

  print(arrWords);
}
