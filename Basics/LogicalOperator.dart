void main() {
  ///Most times we may have to compare values
  ///That can be done by using logical operators
  /**
   * < - less than
   * > - greater than
   * == - is equal
   * <= - less than or equals
   * >= - greater than or equals
   * && - Logical AND
   * || - Logical OR
   */

  var m1 = 10;
  var m2 = 30;
  var m3 = 56;

  print('m1 > m2 ${m1 > m2}');
  print('m1 < m2 ${m1 < m2}');
  print('m1 <= m2 ${m1 <= m2}');
  print('m1 >= m2 ${m1 >= m2}');
  print('m1 == m2 ${m1 == m2}');

  ///Here we are comparing m1 <= m2 and then m2 <= m3
  ///if both are true then true is printed even if
  ///one is false then its final value is false
  print('m1 < m2 AND m2 < m3 ${m1 <= m2 && m2 <= m3}');
}
