///Basic control statements general to all langauges
void main() {
  var number1 = 5;
  var number2 = 10;

  ///Here we make use of English like statements where we compare 2 variables
  ///IF number1 is GREATER THAN number2 print number1
  ///ELSE print number2
  if (number1 > number2) {
    print(number1);
  } else {
    print(number2);
  }

  ///NOTE: IF statement can exist without ELSE but ELSE cannot exist without IF

  ///Complex statements using if else
  var mark1 = 80;
  var mark2 = 61;
  var mark3 = 93;
  var total = mark1 + mark2 + mark3;
  if (total < 40) {
    print("F");
  } else if (total > 40 && total < 90) {
    print("A");
  } else {
    print("O");
  }
}
