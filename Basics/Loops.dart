///Loops are used for iterating
///and doing repeated tasks
void main() {
  ///For loop
  ///for(initializer; condition; operation){}
  int counter;
  for (counter = 1; counter <= 10; counter++) {
    print(counter);
  }

  ///While Loop
  ///while(condition){}
  int iterator = 0;
  while (iterator <= 10) {
    print(iterator);
    iterator++;
  }
}
