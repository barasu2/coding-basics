///Functions are part and parcel of programming and
///They are used to writing reuseble code
void main() {
  ///Regular Function call
  ///prints Hello World!
  helloWorld();

  ///Function with arguments
  ///Prints 3
  add(1, 2);

  ///Function that returns a value in this case 5
  ///which is stored in result and later printed
  int result = sub(10, 5);
  print(result);
}

///Function Type: 1
///No params
void helloWorld() {
  print("Hello World!");
}

///Function Type: 2
///With params
void add(int number1, int number2) {
  print(number1 + number2);
}

///Function Type: 3
///With a return type other than void

int sub(int number1, int number2) {
  ///Return is a keyword used to return
  ///an actual value when called
  return number1 - number2;
}
